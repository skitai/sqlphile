from sqlphile import Q, D, F, V
import pytest

def test_aggregate (sqlmaps):
    sql = (sqlmaps.select ("tbl1 a")
            .join ("tbl2 b", "a.id = b.id")
            .filter (a__id = 4)
            .get ("a.id, b.name")
            .aggregate ('count (a.id) as cnt')
    )

    assert str (sql) == (
        "SELECT count (a.id) as cnt FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )

    sql = (sqlmaps.select ("tbl1 a")
            .join ("tbl2 b", "a.id = b.id")
            .filter (a__id = 4)
            .get ("a.id, b.name")
    )
    sql2 = sql.aggregate ('count (a.id) as cnt')
    assert str (sql2) == (
        "SELECT count (a.id) as cnt FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )
    assert str (sql) == (
        "SELECT a.id, b.name FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )

    sql = (sqlmaps.select ("tbl1 a")
            .join ("tbl2 b", "a.id = b.id")
            .filter (a__id = 4)
            .get ("a.id, b.name")
            .order_by ('-a.id')
            .limit (100)
            .offset (100)
    )
    sql2 = sql.aggregate ('count (a.id) as cnt')
    assert str (sql2) == (
        "SELECT count (a.id) as cnt FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )

    sql = (sqlmaps.select ("tbl1 a")
            .join ("tbl2 b", "a.id = b.id")
            .filter (a__id = 4)
            .get ("a.id, b.name")
            .order_by ('-a.id')
            .limit (100)
    )
    sql2 = sql.aggregate ('count (a.id) as cnt', ignore_limit = False)
    assert str (sql2) == (
        "SELECT count (a.id) as cnt FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4\n"
        "LIMIT 100"
    )



