from sqlphile import Q, D, F, V

def test_embed_sql (sqlmaps):
    sql = (sqlmaps.select ("temp").order_by ('name'))
    assert str (sql) == (
        "SELECT * FROM temp\n"
        "ORDER BY name"
    )

    sql = (sqlmaps.select ("temp").order_by ('+name'))
    assert str (sql) == (
        "SELECT * FROM temp\n"
        "ORDER BY name"
    )

    sql = (sqlmaps.select ("temp").order_by ('-name'))
    assert str (sql) == (
        "SELECT * FROM temp\n"
        "ORDER BY name DESC"
    )

