import sqlphile as sp
from sqlphile import DB_PGSQL, DB_SQLITE3

def test_template (): 
    template = sp.Template (DB_PGSQL)
    q = template.select ("tbl_file").get ("score", "t2.name")
    want = "SELECT score, t2.name FROM tbl_file"
    assert str (q) == q.as_sql () == want    
    
    template = sp.Template (DB_PGSQL, "./sqlmaps/test.sql")
    q = template.test1.filter (name = 'Hans').group_by ("name").order_by ("name").feed (tbl = 'rc_file')    
    want = "select * from rc_file WHERE name = 'Hans'\nGROUP BY name\nORDER BY name"
    assert str (q) == q.as_sql () == want    

    template = sp.Template (DB_PGSQL, "./sqlmaps")
    q = template.test.test1.filter (name = 'Hans').group_by ("name").order_by ("name").feed (tbl = 'rc_file')    
    assert str (q) == q.as_sql () == want    