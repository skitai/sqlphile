from sqlphile import Q, D, F, V

def test_join (sqlmaps):
    sql = sqlmaps.select ("tbl1", "a").join ("tbl2", "b", a__id = F ("b.id"))
    assert str (sql) == (
        "SELECT * FROM tbl1 AS a\n"
        "INNER JOIN tbl2 AS b ON a.id = b.id"
    )

    sql = sqlmaps.select ("tbl1", "a").join ("tbl2", "b", "a.id = b.id")
    assert str (sql) == (
        "SELECT * FROM tbl1 AS a\n"
        "INNER JOIN tbl2 AS b ON a.id = b.id"
    )

    sql = sqlmaps.select ("tbl1", "a").join ("tbl2", "b", "a.id = b.id").filter (a__id = 4)
    assert str (sql) == (
        "SELECT * FROM tbl1 AS a\n"
        "INNER JOIN tbl2 AS b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )

    sql = sqlmaps.select ("tbl1 a").join ("tbl2 b", "a.id = b.id").filter (a__id = 4)
    assert str (sql) == (
        "SELECT * FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )

    sql = sqlmaps.select ("tbl1 a").join ("tbl2 b", a__id = F ("b.id")).filter (a__id = 4)
    assert str (sql) == (
        "SELECT * FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )

    sql = sqlmaps.select ("tbl1 a").join ("tbl2 b", "true").filter (a__id = 4)
    assert str (sql) == (
        "SELECT * FROM tbl1 a\n"
        "INNER JOIN tbl2 b ON true\n"
        "WHERE a.id = 4"
    )

    sql = sqlmaps.select ("tbl1 a").cross_join ("tbl2 b", a__id = F ("b.id")).filter (a__id = 4)
    assert str (sql) == (
        "SELECT * FROM tbl1 a\n"
        "CROSS JOIN tbl2 b ON a.id = b.id\n"
        "WHERE a.id = 4"
    )

    sql = sqlmaps.select ("tbl1 a").cross_join ("tbl2 b", on = 'true').filter (a__id = 4)
    assert str (sql) == (
        "SELECT * FROM tbl1 a\n"
        "CROSS JOIN tbl2 b ON true\n"
        "WHERE a.id = 4"
    )

